#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>
#include<time.h>

int status, status2;

int main(){
    pid_t char_ch;
    char_ch = fork();
    if(char_ch < 0){
        exit(EXIT_FAILURE);
    }else if(!char_ch){
            execl("/usr/bin/wget", "wget", "-O", "./binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "-qq", NULL);
            exit(0);
    }else{
        wait(0);
        char_ch = fork();
        if(char_ch < 0){
            exit(EXIT_FAILURE);
        }else if(!char_ch){
            char *argv[] = {"unzip", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang.zip", "-d", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang", NULL};
            execv("/usr/bin/unzip", argv);
        }else{
            wait(0);
            char_ch = fork();
            if(char_ch < 0){
                exit(EXIT_FAILURE);
            }else if(!char_ch){
                DIR *dp;
                struct dirent *ep;
                char path[100] = {"/home/anneutsabita/SISOP/praktikum2/soal1/binatang"};
                dp = opendir(path);
                int picture_count;
                char picture_files[9][256];
                
                if(dp){
                    while ((ep = readdir (dp)) != NULL && picture_count < 9){
                        if(strstr(ep->d_name, ".jpg")){
                            strcpy(picture_files[picture_count], ep->d_name);
                            picture_count++;
                        }
                    }
                }
                closedir(dp);

                printf("\nIt's the shift to look after: ");
                for(int i = 0; i < 1; i++){
                    int random_index = rand() % picture_count;
                    printf("%s\n\n", picture_files[random_index]);
                }
                
            }else{
                wait(0);
                char_ch = fork();
                if(char_ch < 0){
                    exit(EXIT_FAILURE);
                }else if(!char_ch){
                    char_ch = fork();
                    if(char_ch < 0){
                        exit(EXIT_FAILURE);
                    }else if(!char_ch){
                        execl("/bin/mkdir", "mkdir", "-p", "/home/anneutsabita/SISOP/praktikum2/soal1/HewanDarat", NULL);
                    }else{
                        wait(0);
                        execl("/bin/mkdir", "mkdir", "-p", "/home/anneutsabita/SISOP/praktikum2/soal1/HewanAmphibi", NULL);
                    }
                }else{
                    wait(0);
                    char_ch = fork();
                    if(char_ch < 0){
                        exit(EXIT_FAILURE);
                    }else if(!char_ch){
                        char_ch = fork();
                        if(char_ch < 0){
                            exit(EXIT_FAILURE);
                        }else if(!char_ch){
                            execl("/bin/mkdir", "mkdir", "-p", "/home/anneutsabita/SISOP/praktikum2/soal1/HewanAir", NULL);
                        }else{
                            wait(0);
                            execl("/bin/mkdir", "mkdir", "-p", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang", NULL);
                            exit(0);
                        }
                    }else{
                        wait(0);
                        char_ch = fork();
                        if(char_ch < 0){
                            exit(EXIT_FAILURE);
                        }else if(!char_ch){
                            char *argv[] = {"unzip", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang.zip", "-d", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang", NULL};
                            execv("/usr/bin/unzip", argv);
                            exit(0);
                        }else{
                            wait(0);
                            char_ch = fork();
                            if(char_ch < 0){
                                exit(EXIT_FAILURE);
                            }else if(!char_ch){
                                DIR *dp;
                                struct dirent *ep;
                                char path[100] = {"/home/anneutsabita/SISOP/praktikum2/soal1/binatang"};
                                dp = opendir(path);

                                if(dp){
                                    while ((ep = readdir (dp)) != NULL){
                                        if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) continue;
                                        char namafile[350];
                                        sprintf(namafile, "/home/anneutsabita/SISOP/praktikum2/soal1/binatang/%s", ep->d_name);
                                        if(strstr(ep->d_name, "darat")){
                                            if(!fork()){
                                                char *argv[] = {"mv", namafile, "/home/anneutsabita/SISOP/praktikum2/soal1/HewanDarat", NULL};
                                                execv("/bin/mv", argv);
                                            }
                                        }else if(strstr(ep->d_name, "amphibi")){
                                            if(!fork()){
                                                char *argv[] = {"mv", namafile, "/home/anneutsabita/SISOP/praktikum2/soal1/HewanAmphibi", NULL};
                                                execv("/bin/mv", argv);
                                            }
                                        }else if(strstr(ep->d_name, "air")){
                                            if(!fork()){
                                                char *argv[] = {"mv", namafile, "/home/anneutsabita/SISOP/praktikum2/soal1/HewanAir", NULL};
                                                execv("/bin/mv", argv);
                                            }
                                        }
                                        while ((wait(&status)) > 0);
                                    }
                                    pid_t rm_unzip;
                                    rm_unzip = fork();
                                    if(!rm_unzip){
                                        char *argv[] = {"rm", "-r", "/home/anneutsabita/SISOP/praktikum2/soal1/binatang", NULL};
                                        execv("/bin/rm", argv);
                                    }
                                    closedir(dp);
                                    while ((wait(&status2)) > 0);
                                }
                            }else{
                                wait(0);
                                sleep(10);
                                char_ch = fork();
                                if(char_ch < 0){
                                    exit(EXIT_FAILURE);
                                }else if(!char_ch){
                                    char_ch = fork();
                                    if(char_ch < 0){
                                        exit(EXIT_FAILURE);
                                    }else if(!char_ch){
                                        execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
                                    }else{
                                        wait(0);
                                        char_ch = fork();
                                        if(char_ch < 0){
                                            exit(EXIT_FAILURE);
                                        }else if(!char_ch){
                                            execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
                                        }else{
                                            wait(0);
                                            execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
                                        }
                                    }
                                }else{
                                    wait(0);
                                    char_ch = fork();
                                    if(char_ch < 0){
                                        exit(EXIT_FAILURE);
                                    }else if(!char_ch){
                                        char command[500];
                                        sprintf(command, "rm -r HewanDarat HewanAmphibi HewanAir");
                                        system(command);
                                    }else{
                                        wait(0);
                                        char command[500];
                                        sprintf(command, "rm binatang.zip");
                                        system(command);
                                        exit(0);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }	        
}
