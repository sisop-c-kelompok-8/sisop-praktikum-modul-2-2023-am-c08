# PRAKTIKUM 2 KELOMPOK 8 SISOP C


### Praktikum Sisop Modul 2
Group Members:
1. Keysa Anadea (5025211028)
2. Anneu Tsabita (5025211026)
3. Dimas Pujangga (5025211212)

<h2>Daftar Isi</h2>

- [Question 1](#question-1) <br>
	- [Question 1a](#1a)
	- [Question 1b](#1b)
	- [Question 1c](#1c)
	- [Question 1d](#1d)
	- [Source Code Question 1](#source-code-question-1)
	- [Output Question 1](#output-question-1)
- [Question 2](#question-2) <br>
	- [Question 2a](#2a)
	- [Question 2b](#2b)
	- [Question 2c](#2c)
	- [Question 2d](#2d)
	- [Question 2e](#2e)
	- [Source Code Question 2](#source-code-question-2)
	- [Output Question 2](#output-question-2)
- [Question 3](#question-3) <br>
	- [Question 3a](#3a)
	- [Question 3b](#3b)
	- [Question 3c](#3c)
	- [Question 3d](#3d)
	- [Source Code Question 3](#source-code-question-3)
	- [Output Question 3](#output-question-3)
- [Question 4](#question-4) <br>
	- [Source Code Question 4](#source-code-question-4)
	- [Penjelasan Question 4](#penjelasan-question-4)
	- [Output Question 4](#output-question-4)
    

## Question 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq     
**Catatan :**
- untuk melakukan zip dan unzip tidak boleh menggunakan system


#### 1A.
**_SOAL :_**    
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.    

**_Pengaplikasian Pada Program:_**  
![1a](/uploads/803be76a4714bf071dfb1f8d32933552/1a.jpg)
**_Penjelasan Kode:_**
Karena masing-masing perintah menggunakan bash command, dan tidak boleh menggunakan system(), maka akan digunakan exec. Karena exec akan menghentikan proses saat ini, tiap-tiap perintah akan ditaruh di percabangan fork() yang telah dibuat yang banyaknya sesuai banyaknya proses yang akan kita jalankan.

Setelah dilakukan fork(), pada child process akan mendownload file zip yang ada pada soal dengan menggunakan execl secara diam (progress mendownload tidak terlihat) dengan menggunakan '-qq'. File zip tersebut dinamakan binatang.zip. Lalu pada parent process akan menunggu child process selesai dengan 'wait()'. Setelah child process selesai akan dilanjtkan ke parent process, yaitu unzip file zip yang sudah didownload menggunakan execv. Perintah unzip diinisialisasi terlebih dahulu ke dalam argumen argv, dan setelah itu akan dijalankan dengan execv. File yang sudah di-unzip menghasilkan folder binatang.

#### 1B.
**_SOAL :_**    
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.   

**_Pengaplikasian Pada Program:_**  
![1b](/uploads/12965268e27e5ae164c7a3348f55c99a/1b.jpg)
**_Penjelasan Kode:_**
Untuk memilih file secara acak pada file dalam folder binatang, pertama-tama folder binatang tersebut akan dibuka terlebih dahulu dengan menggunakan 'DIR' dengan path folder tersebut. File-file yang ada dalam folder binatang akan dibaca namanya secara rekursif dengan menggunakan perulangan while. Selama 'picture_count' kurang dari 9, file dalam folder akan dibaca dan dicek apakah file tersebut merupakan file 'jpg'. Jika iya, maka nama file tersebut akan dimasukan ke dalam array 'picture_files' dan 'picture_count' akan bertambah satu setiap perulangan dilakukan.

Setelah perulangan while selesai, dir akan ditutup. Setelah itu akan dilakukan pemilihan secara acak pada array 'picture_files' dengan menggunakan 'rand()'. Hasil pemilihan akan muncul pada terminal.

#### 1C.
**_SOAL :_**    
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.    

**_Pengaplikasian Pada Program:_**  
![1c_2_](/uploads/13056b30d7cbd1a15ebb8b210bab6ad6/1c_2_.jpg) 

**_Penjelasan Kode:_**
Gambar-gambar pada folder ``binatang`` akan dipisahkan sesuai nama gambarnya. Contohnya untuk file gambar dengan nama ``darat`` akan masuk ke dalam folder ``HewanDarat``. Begitu juga dengan gambar-gambar lainnya. Untuk itu, akan dibuat folder baru bernama ``HewanDarat``, ``HewanAmphibi``, dan ``HewanAir``. Setelah itu, dengan cara yang sama seperti soal 1b, untuk memisahkan gambar yang ada ke dalam folder sesuai namanya, folder ``binatang`` akan dibuka terlebih dahulu dengan menggunakan ``DIR``. File-file yang ada dalam folder ``binatang`` akan dibaca namanya secara rekursif dengan menggunakan perulangan ``while()``. 

Nama file yang sudah dibaca akan dimasukkan path-nya ke dalam ``namafile`` untuk dipindahkan nanti. File-file yang sudah dibaca akan dicari apakah mengandung kata-kata ``darat`` atau ``amphibi`` atau ``air`` dengan menggunakan ``strstr()``. Kalau sudah, gambar-gambar tersebut akan dipindahkan ke dalam folder yang sudah dibuat tadi sesuai dengan namanya menggunakan ``execv()`` yang perintahnya akan dimasukkan ke dalam ``argv`` terlebih dahulu. Setelah gambar-gambarnya berhasil dipindahkan, folder binatang yang sudah kosong akan diremove atau didelete menggunakan ``execv()`` dengan perintah ``rm``.


#### 1D.
**_SOAL :_**    
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan    

**_Pengaplikasian Pada Program:_** 

![1d](/uploads/f8d3dbbfac971bbfbd479ef8b6237c7d/1d.jpg)

**_Penjelasan Kode:_**
Folder ``HewanDarat``, ``HewanAmphibi``, dan ``HewanAir`` yang sudah diisi sebelumnya dengan gambar-gambar akan dizip menggunakan ``execlp()`` dengan peintah ``zip``. Perintah ``zip`` tersebut dilakukan secara rekursif dari direktori yang di-zip dengan menggunakan argumen ``-r``.

#### Source Code Question 1
![code1](/uploads/1a48c56ebac26360d2aca32962d51197/code1.jpg)
![code2](/uploads/6d8c1845c165e6b7e69b59f39320fa96/code2.jpg)
![code3](/uploads/7a60dc3f39b99cf8720d132e17ed05cd/code3.jpg)
![code4](/uploads/1a29f44ad75d89bae838061e30905ee6/code4.jpg)
![code5](/uploads/f6600c8146de335b5e7d83cb8185176e/code5.jpg)

#### Output Question 1
**_Compile Program :_**

![compile1](/uploads/dbab3a6ece53a046455ab9ec1b3fca99/compile1.jpg)
![compile2](/uploads/64135d568094ccfa47912038dd303de6/compile2.jpg)

**_Output 1a :_**

![out1a](/uploads/4a0a90bb3e87343d0541a85133999785/out1a.jpg)

**_Output 1b :_**

![out1b](/uploads/a80a699a672ff79894ddcf40132a8337/out1b.jpg)

**_Output 1c :_**

![out1c](/uploads/aeb2f0d9f17e37fd0f6db30f32666641/out1c.jpg)

**_Output 1d :_**

![out1d](/uploads/5e1dfc3518904c9960106e4d3774a500/out1d.jpg)


## Question 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !  
**Catatan :**
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

#### 2A.
**_SOAL :_**      
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].    
**_Pengaplikasian Pada Program:_**  
![2b](/uploads/cd4c07bd0a28e46944f2161e6a8af41b/2b.jpg)     
![2a](/uploads/ebfc6d78d0aadbad36459a36ace217ff/2a.jpg)          
**_Penjelasan Kode:_**
Pada bagian ini, pertama-tama dilakukan pengambilan waktu menggunakan fungsi time dari library ``time.h``. Selanjutnya waktu tersebut diubah formatnya menjadi string dengan menggunakan fungsi ``strftime``. Format yang digunakan adalah %Y-%m-%d_%H:%M:%S, sehingga nama folder yang dihasilkan akan berformat YYYY-MM-dd_HH:mm:ss.

Selanjutnya, program melakukan ``fork`` untuk membuat child process yang akan menjalankan perintah untuk membuat folder dengan menggunakan perintah ``mkdir``. Folder tersebut akan diberi nama sesuai dengan variabel namaFolder yang telah diinisialisasi sebelumnya. Proses child kedua kemudian menunggu hingga proses child pertama selesai melakukan pembuatan folder.

#### 2B.
**_SOAL :_**    
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].      
**_Pengaplikasian Pada Program:_**  
![messageImage_1680876769943](/uploads/a08e1d1a8400492f4979a6b168a6c1c0/messageImage_1680876769943.jpg)     
**_Penjelasan Kode:_**      
Pada bagian ini, dilakukan loop sebanyak 15 kali untuk mengunduh 15 gambar dari https://picsum.photos/. Setiap iterasi dalam loop, akan dilakukan proses ``fork()`` untuk membuat proses baru yang akan mengunduh satu gambar.

Pada proses child, pertama dilakukan perpindahan direktori ke dalam folder yang telah dibuat sebelumnya dengan menggunakan fungsi ``chdir()``. Selanjutnya, dilakukan penghitungan ukuran gambar berdasarkan detik Epoch Unix dengan menggunakan fungsi ``time()`` dan menghitung (t%1000)+50. Hasilnya disimpan dalam variabel cnt.

Selanjutnya, dilakukan pembentukan nama file gambar dengan menggunakan format timestamp [YYYY-mm-dd_HH:mm:ss] dengan menggunakan fungsi ``strftime()``. Nama file gambar disimpan dalam variabel namaFile.

Terakhir, dilakukan eksekusi command untuk mengunduh gambar dengan menggunakan perintah ``wget``. URL gambar dan nama file yang telah dibentuk sebelumnya dijadikan parameter pada command ``wget``. Gambar yang diunduh disimpan dalam folder dengan nama dan path yang telah ditentukan. Proses mengunduh gambar di-delay selama 5 detik sebelum iterasi selanjutnya dimulai.

#### 2C.
**_SOAL :_**    
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).     
**_Pengaplikasian Pada Program:_**  
![2c](/uploads/5e6e252bce51498dc109dc5aa6f8b760/2c.jpg)      
**_Penjelasan Kode:_** 
Pada kode di atas, pertama-tama dibuat sebuah string namaFolderZIP yang berisi nama folder dengan tambahan ekstensi .zip. Kemudian, dibuat sebuah array of strings ``argvzip`` yang berisi argumen-argumen yang akan digunakan untuk menjalankan program zip pada direktori /usr/bin/zip. Argumen-argumen tersebut adalah:

- ``qrm``: opsi untuk menjalankan program zip secara quiet, yaitu tanpa menampilkan output ke layar, dan memproses seluruh subdirektori dari direktori yang akan di-zip.
- ``namaFolderZIP``: nama file zip yang akan dibuat.
- ``namaFolder``: nama direktori yang akan di-zip.

Setelah itu, program menjalankan perintah ``execv("/usr/bin/zip", argvzip)``, yang berarti menjalankan program zip dengan argumen-argumen yang sudah diatur sebelumnya pada direktori /usr/bin/zip. 

#### 2D.
**_SOAL :_**    
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.     
**_Pengaplikasian Pada Program:_**
![2d](/uploads/8b713fcc152556feed7ee958e37dbe32/2d.jpg)      
**_Penjelasan Kode:_**
Untuk membuat program "killer", pertama-tama dibuka file "killer.sh" dengan mode "write" menggunakan fungsi ``fopen()``. Kemudian, di file tersebut ditulis script Bash menggunakan fungsi ``fprintf()`` yang berisi perintah ``kill -9`` untuk menterminate program utama dan ``rm killer`` untuk menghapus file "killer" setelah program utama berhasil dihentikan.

Selain itu, pada bagian pembuatan file "killer", ditambahkan opsi ``-a`` dan ``-b``. Opsi ``-a`` digunakan untuk menterminate semua proses yang terkait dengan session id ``(sid)`` yang sama dengan program utama, sementara opsi ``-b`` digunakan untuk menterminate proses yang memiliki process id ``(pid)`` yang sama dengan program utama.

Setelah menulis script "killer", file "killer.sh" ditutup menggunakan fungsi ``fclose()``. Selanjutnya, dibuat child process menggunakan fungsi ``fork()`` yang akan menjalankan perintah-perintah selanjutnya. Pertama-tama, menggunakan fungsi ``execv()`` untuk menjalankan perintah ``chmod u+x killer.sh`` yang berfungsi untuk memberikan izin executable pada file killer.sh. Selanjutnya, menggunakan fungsi ``wait()`` untuk menunggu child process selesai dijalankan. 

Kemudian, menggunakan fungsi ``fork()`` lagi untuk membuat child process yang kedua. Kali ini, child process tersebut akan menjalankan perintah ``mv killer.sh killer`` yang berfungsi untuk mengubah nama file "killer.sh" menjadi "killer". Setelah itu, menggunakan fungsi ``wait()`` untuk menunggu child process selesai dijalankan.

Terakhir, menggunakan fungsi ``close()`` untuk menutup stdin, stdout, dan stderr, sehingga program tidak akan terus menampilkan output yang tidak diperlukan. 

#### 2E.
**_SOAL :_**    
Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).   
**_Pengaplikasian Pada Program:_**  
![2e](/uploads/4f4349ce5dada9db6d38c36d4a81598f/2e.jpg)     
**_Penjelasan Kode:_**
Pada bagian ini, kita menggunakan argumen yang diberikan saat program dijalankan ``(argv[1])`` untuk menentukan mode yang dipilih (-a atau -b). Jika mode yang dipilih adalah ``MODE_A (-a)``, program akan mencetak perintah untuk menghentikan semua proses (termasuk child process) dengan menggunakan perintah ``kill -9 -[sid]`` dan menghapus file killer.sh (yang digunakan untuk menghentikan program) setelah selesai. Sedangkan jika mode yang dipilih adalah ``MODE_B (-b)``, program akan mencetak perintah untuk menghentikan semua proses (termasuk child process) dengan menggunakan perintah ``kill -9 [pid]`` (pid adalah id process program utama) dan menghapus file killer.sh setelah selesai.

Setelah itu, program akan mengubah izin akses file killer.sh dengan menggunakan perintah ``chmod`` dan memindahkan file killer.sh ke file killer dengan menggunakan perintah ``mv``.

Jika program dijalankan dalam ``MODE_A``, program akan dihentikan ketika file killer dijalankan. Jika program dijalankan dalam ``MODE_B``, program akan tetap berjalan sampai proses di semua folder selesai dan dihentikan ketika file killer dijalankan.

#### Source Code Question 2
![messageImage_1680880698347](/uploads/80a7ac13444af23fc4a500078394d751/messageImage_1680880698347.jpg)
![sc2](/uploads/6d934d14e1a5bf5b3cfec92f91584ac4/sc2.jpg)
![sc3](/uploads/5e9d322384338ff441d91dd5bf87bdd1/sc3.jpg)
![sc4](/uploads/43755351afc0d4534434e778f368475c/sc4.jpg)

#### Output Question 2
##### Mode A        
Perhatikan waktu pada screenshot dengan seksama.         
**_Compile Program :_**   
![acom](/uploads/6452fd98286d59ed6c3c426230800443/acom.jpg)    

**_Contoh Folder :_**  
![foldera](/uploads/025552674d11cd9cb102c8471988abdd/foldera.jpg)    

**_Contoh Isi :_**      
![isia](/uploads/1c71bf6f5cf8bb9eb05f69af1d6c889a/isia.jpg)     

**_Contoh Zip :_**      
![zipa](/uploads/5a4a3cf4b75a945bbb631d8f73a43029/zipa.jpg)     

**_Compile Killer :_**      
![killera](/uploads/83b4981c8f1df4703edded0c6cc15b63/killera.jpg)       

**_Program setelah di kill :_**  
Setelah di kill, semua program akan terminate tanpa terkecuali, dan file killer akan terhapus dengan sendirinya. Pada screenshot output pada dibawah ini waktu sudah berjalan 3 menit setelah program di kill. Hasil mode -a semua program berhenti dan tidak berjalan sehingga terdapat folder yang tidak terzip.                              
![hasilkillera](/uploads/040a7d4a4b2056523047384b5729d7b5/hasilkillera.jpg)



##### Mode B        
Perhatikan waktu pada screenshot dengan seksama.        
**_Compile Program :_**     
![bcom](/uploads/dae1c70fdc8827ce23171122d4dcb7f1/bcom.jpg)     

**_Contoh Folder :_**       
![bfolder](/uploads/66973b0663a41bbade5201dd4df5b6de/bfolder.jpg)       

**_Contoh Isi :_**           
![bisi](/uploads/870f0d38c02937e416a5cbbd2f0e20bc/bisi.jpg)     

**_Contoh Zip :_**            
![bzip](/uploads/c1bb49a84dc5045706511d54bbc3459d/bzip.jpg)     

**_Compile Killer :_**           
![bkiller](/uploads/a63d2c16a90056cb3937d3617a19084b/bkiller.jpg)       

**_Program setelah di kill :_**                
Setelah di kill, program akan berhenti mengenerate new folder **TETAPI** tetap menjalankan semua folder yang dibuat sampai folder terzip, dan file killer akan terhapus dengan sendirinya. Pada screenshot output pada dibawah ini waktu sudah berjalan 1 menit setelah program di kill. Hasil mode -b semua folder tetap akan berjalan sampai ter zip walaupun sudah di kill.            
![bhasil](/uploads/4cadd8ddd644805ee6e48c6a24c76ef2/bhasil.jpg)     


## Question 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”    
**Catatan:**
- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

#### 3A.
**_SOAL :_**    
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.     
**_Pengaplikasian Pada Program:_**  
![image](/uploads/665d3d159af0a30092505d947d241bf4/image.png)


**_Penjelasan Kode:_**
Karena masing-masing perintah menggunakan bash command, dan tidak boleh menggunakan system(), maka akan digunakan exec. Karena exec akan menghentikan proses saat ini, tiap-tiap perintah akan ditaruh di percabangan fork() yang telah dibuat yang banyaknya sesuai banyaknya proses yang akan kita jalankan.
Setelah dilakukan fork(), pada child process akan mendownload file zip yang ada pada soal dengan menggunakan execl secara diam (progress mendownload tidak terlihat) dengan menggunakan '-qq'. File zip tersebut dinamakan players.zip. Lalu pada parent process akan menunggu child process selesai dengan 'wait()'. Setelah child process selesai akan dilanjutkan ke parent process, yaitu unzip file zip yang sudah didownload menggunakan execv. Perintah unzip diinisialisasi terlebih dahulu ke dalam argumen argv, dan setelah itu akan dijalankan dengan execv. File yang sudah di-unzip menghasilkan folder players. Terakhir, menghapus file players.zip menggunakan execl.

#### 3B.
**_SOAL :_**    
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.   
**_Pengaplikasian Pada Program:_**  
![image](/uploads/c38a1e8dfa8c0bea735f0f9072cd2c26/image.png)


**_Penjelasan Kode:_**
Pertama-tama dibuka terlebih dahulu directory players, lalu akan dicompare file yang bernama '.' dan '..' akan diabaikan karena dalam ketentuan soal nama file tersebut tidak termasuk directory yang dihapus. Lalu, jika nama file yang tidak memiliki unsur nama "ManUtd" akan dihapus menggunakan execl dan 'rm'. Lalu apabila semua proses sudah dilakukan hingga tidak ada lagi file yang tersisa, directory akan diclose menggunakan closedir(dp).

#### 3C.
**_SOAL :_**    
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.     
**_Pengaplikasian Pada Program:_**  
![image](/uploads/cb014279f3e65f84195102e647f58072/image.png)
![image](/uploads/ad1895840f6aa83923d6f03f5a28a06a/image.png)


**_Penjelasan Kode:_**
Untuk membuat directory menggunakan perintah execl dan 'mkdir' sebanyak 4 folder yaitu Kiper, Bek, Gelandang, dan Penyerang. Kemudian, untuk mengkategorikan pemain sesuai posisinya kami menggunakan fungsi yang bernama categorize. Fungsi tersebut akan mencocokkan nama file dari posisi pemain dengan nama folder posisi pemain, jika sudah sesuai akan di chdir ke folder posisi masing-masing pemain, jika bek maka akan masuk ke folder bek, dan seterusnya.

#### 3D.
**_SOAL :_**    
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/    
**_Pengaplikasian Pada Program:_**  
![image](/uploads/3d4a10f46c88660da904949b2adac27d/image.png)
![image](/uploads/da9c2ab1629a8d1f3a1b5c1ebc930580/image.png)
![image](/uploads/57832f8b76a3ccbbf288f07a3f2a35d7/image.png)


**_Penjelasan Kode:_**
Sesuai ketentuan soal, nomor 3D diharuskan membuat fungsi yang bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker. Pertama menggunakan printf dan scanf untuk menginput formasi terbaik sesuai overall rating pemain. Kemudian pada fungsi buatTim, pertama dibuat file txt untuk menyimpan hasil output formasi. Lalu ada struct Players dan int count. Kemudian, ada fungsi list_players yang isinya pertama membuka directory players lalu mengabaikan nama file '.' dan '..' selanjutnya mengesktrak rating pemain di sscanf, dan mengcopy nama file kedalam argumen players dan hasil rating yang sudah diesktrak dimasukan ke players. Lalu, ada qsort untuk mensorting pemain sesuai posisinya,terakhir akan diprint hasil formasi terbaik yang sudah diinput ke dalam file txt.

#### Source Code Question 3
![Screenshot_2023-04-08_163151](/uploads/01b92d44e84b39e0d7a3e155f762ddea/Screenshot_2023-04-08_163151.png)
![Screenshot_2023-04-08_163205](/uploads/85adab9e119ecc3be1f9a0960c8bb37b/Screenshot_2023-04-08_163205.png)
![Screenshot_2023-04-08_163230](/uploads/5210b83ed9b20a19afe7171199a76f55/Screenshot_2023-04-08_163230.png)
![Screenshot_2023-04-08_163249](/uploads/5fc27ab54ff95ed8746b189ff7947697/Screenshot_2023-04-08_163249.png)
![Screenshot_2023-04-08_163304](/uploads/365e818e11927eb6d70bf2ef0c6fed9f/Screenshot_2023-04-08_163304.png)

#### Output Question 3
Ketika program dirun, kita akan menginput formasi sesuai keinginan kita.

![image](/uploads/19b426a41cd92ba124ba080d59c1ef08/image.png)

Folder players:
![image](/uploads/1467097c5f3e33dc9c11b139a473b930/image.png)

Isi dari folder players:
![image](/uploads/679792e50511c735f1e6c66714909173/image.png)

Isi dari folder gelandang:
![image](/uploads/0476adc62f3d7c5f102b038db7ddab49/image.png)

Hasil output formasi berupa file .txt berada di home sesuai ketentuan soal:
![image](/uploads/4470a5a780aea482adcb0d2d2ee62206/image.png)

Isi file formasi.txt:
![image](/uploads/96a602ce533f5d27af4720954ae2c949/image.png)


## Question 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.

#### Source Code Question 4
![4code1](/uploads/dba592afec77e14f46cd584736fe152e/4code1.jpg)
![4code2](/uploads/3f1ea68ae6d797e16b7a8861115e8433/4code2.jpg)
![4code3](/uploads/b96d2a151a65b4c46b4297a3a26c6a3f/4code3.jpg)

#### Penjelasan Question 4
Pada soal ini diminta untuk membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file ``.sh``. Sehingga pada saat mengcompile, akan diterima 5 argumen. Jika yang ditulis user kurang dari 5 argumen, program akan mengeluarkan error message ke terminal karena argumen yang di berikan tidak sesuai. Begitu juga jika argumen hour, minute, dan detik yang ditulis tidak sesuai.

Program ini menggunakan Daemon Process agar dapat berjalan di background secara terus menerus tanpa adanya interaksi secara langsung dengan user yang sedang aktif. Untuk melakukan Daemon Process, langkah pertama yang harus dilakukan adalah membuat sebuah parent process dan memunculkan child process dengan melakukan ``fork()``. Kemudian bunuh parent process agar sistem operasi mengira bahwa proses telah selesai.

Kemudian langkah selanjutnya adalah membuat Unique Session ID (SID). Sebuah Child Process harus memiliki SID agar dapat berjalan. Tanpa adanya SID, Child Process yang Parent-nya sudah di-kill akan menjadi Orphan Process. Untuk mendapatkan SID kita dapat menggunakan perintah ``setsid()``. Perintah tersebut memiliki return type yang sama dengan perintah ``fork()``.

Lalu, working directory harus diubah ke suatu directory yang pasti ada. Untuk amannya, kita akan mengubahnya ke root (/) directory karena itu adalah directory yang dijamin ada pada semua distro linux. Untuk mengubah Working Directory, kita dapat menggunakan perintah ``chdir()``.

Kemudian, akan dilakukan pengalihan output dari program ke terminal ``(/dev/tty)``. Program membuka file ``/dev/tty`` dengan mode ``O_WRONLY`` untuk menulis ke terminal. File ``/dev/tty`` adalah file karakter khusus yang mewakili terminal saat ini. Program menggunakan fungsi ``dup2()`` untuk menyalin file descriptor fd ke ``STDOUT_FILENO`` (file descriptor standar output). Artinya, semua output yang seharusnya ditampilkan di ``stdout`` akan ditampilkan di terminal ``(/dev/tty)``. Terakhir, program menutup file descriptor ``fd`` karena sudah tidak diperlukan lagi.

Setelah pengalihan output ke terminal, program kemudian menjalankan "cron job". Program mendeklarasi variabel ``current_time`` dengan tipe data ``time_t``, yang digunakan untuk menyimpan nilai waktu saat ini dalam format epoch time (jumlah detik sejak 1 Januari 1970).Berikutnya, program menggunakan fungsi ``localtime()`` untuk mengkonversi waktu saat ini ke format waktu lokal dan menyimpannya dalam variabel ``local_time`` dengan tipe data struct ``tm``. Setelah itu, program dapat melakukan operasi atau tugas lain yang diperlukan dalam cron job sesuai dengan kebutuhan aplikasi.

Setelah itu pada program ini akan membuat Loop Utama dengan menggunakan perulangan ``while()``. Di loop utama ini lah tempat untuk menuliskan inti program. Dalam loop utama ini, dilakukan sebuah cron job. Cron job adalah sebuah tugas yang dijadwalkan untuk dijalankan pada waktu tertentu. Pada loop ini, program akan mengambil waktu saat ini, memeriksa apakah waktu tersebut sesuai dengan jadwal cron yang telah ditentukan, dan jika sesuai, program akan menjalankan sebuah script atau perintah shell.

Pada setiap iterasi loop, program akan terlebih dahulu mendapatkan waktu saat ini dengan menggunakan fungsi ``time(NULL)`` dan menyimpannya dalam variabel ``current_time`` yang bertipe data ``time_t``. Selanjutnya, program menggunakan fungsi ``localtime()`` untuk mengkonversi waktu saat ini ke dalam format waktu lokal dan menyimpannya dalam variabel ``local_time`` yang bertipe data struct ``tm``.

Setelah itu, program akan memeriksa apakah waktu saat ini sesuai dengan jadwal cron yang telah ditentukan. Untuk melakukan ini, program akan membandingkan waktu saat ini dengan waktu yang diatur dalam variabel hour, minute, dan second yang telah diinisialisasi sebelumnya. Jika waktu saat ini sesuai dengan jadwal cron, program akan menjalankan script atau perintah shell yang telah ditentukan dengan menggunakan fungsi ``system()`` dan menyimpan hasilnya dalam variabel ``ret``.Jika terdapat kesalahan saat menjalankan script atau perintah shell, program akan menampilkan pesan error dan keluar dari program dengan memanggil fungsi ``exit()`` dengan argumen ``EXIT_FAILURE``.

Setelah menyelesaikan pengecekan jadwal cron dan menjalankan script, program akan menjeda atau ``sleep`` selama 1 detik dengan menggunakan fungsi ``sleep(1)`` sebelum mengulang kembali proses pengecekan jadwal cron. Loop ini akan terus berjalan sampai program dihentikan secara manual.

#### Output Question 4
**_File sh :_**

![sh](/uploads/7e774e8eeb56cb44b6a639e1949d9553/sh.jpg)

**_Compile Program :_**

![compile4](/uploads/35614cf96f1bba8fd3ce13bbde209d4a/compile4.jpg)
![compile42](/uploads/c204acffffa7f63f1f22da2b1c9b1374/compile42.jpg)

**_Run Program :_**

![run4](/uploads/6d48f05bc9aeb31e9543c88e45a3649f/run4.jpg)

**_Output :_**

![out4](/uploads/544fdc9812818cc4852c36e84a75cd72/out4.jpg)

**_Output after a while :_**
![out42](/uploads/c3cf77436bb7b09a48a5b0d0310f9ae5/out42.jpg)
