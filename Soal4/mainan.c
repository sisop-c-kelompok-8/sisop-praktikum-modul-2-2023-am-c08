#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, char *argv[]) {
    if (argc != 5) {
        printf("Error: Invalid number of arguments\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Parse the arguments
    char* hour = argv[1];
    char* minute = argv[2];
    char* second = argv[3];
    char* script_path = argv[4];

    // Check if the arguments are valid
    if ((strcmp(hour, "*") != 0 && (atoi(hour) < 0 || atoi(hour) > 23))
        || (strcmp(minute, "*") != 0 && (atoi(minute) < 0 || atoi(minute) > 59))
        || (strcmp(second, "*") != 0 && (atoi(second) < 0 || atoi(second) > 59))) {
        printf("Error: Invalid time format\n");
        printf("Usage: %s [0-23/*] [0-59/*] [0-59/*] [path_to_script.sh]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    // Fork the process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork failed\n");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // Parent process
        printf("Cron job started with PID %d\n", pid);
        exit(EXIT_SUCCESS);
    }

    // Child process

    // Create a new session
    if (setsid() < 0) {
        printf("Error: setsid() failed\n");
        exit(EXIT_FAILURE);
    }

    // Set the working directory to root
    if (chdir("/") < 0) {
        printf("Error: chdir() failed\n");
        exit(EXIT_FAILURE);
    }

    // Redirect standard output to the terminal
    int fd = open("/dev/tty", O_WRONLY);
    dup2(fd, STDOUT_FILENO);
    close(fd);

    // Run the cron job
    time_t current_time;
    struct tm *local_time;

    while (1) {
        // Get the current time
        current_time = time(NULL);
        local_time = localtime(&current_time);

        // Check if the time matches the cron schedule
        if ((strcmp(hour, "*") == 0 || local_time->tm_hour == atoi(hour))
            && (strcmp(minute, "*") == 0 || local_time->tm_min == atoi(minute))
            && (strcmp(second, "*") == 0 || local_time->tm_sec == atoi(second))) {
            // Run the script
            int ret = system(script_path);
            if (ret < 0) {
                printf("Error: Failed to execute script\n");
                exit(EXIT_FAILURE);
            }
        }

        // Sleep for 1 second
        sleep(1);
    }

    return 0;
}
