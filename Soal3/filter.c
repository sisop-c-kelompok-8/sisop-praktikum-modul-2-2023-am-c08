#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>
#include<time.h>

int status, status2, status3, status4, status5;
int i = 0;

typedef struct{
    char listname[300];
    char name[50];
    char team[50];
    char position[50];
    int rating;
}Player;

int cmp_func(const void * a, const void * b) {
  return ((Player * ) b) -> rating - ((Player * ) a) -> rating;
}

void list_players(const char * position, Player * players, int * count){
    DIR *dp;
    struct dirent *ep;
    char path[100];
    sprintf(path, "./players/%s", position);
    dp = opendir(path);

    if(dp){
        while ((ep = readdir (dp)) != NULL){
            if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) continue;
            int rating;
            sscanf(ep -> d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", & rating);
            strcpy(players[ * count].listname, ep -> d_name);
            players[ * count].rating = rating;
            ( * count) ++;
        }
        closedir(dp);
        }
}

void buatTim(int bek, int gelandang, int penyerang){
    char txtpath[300];
    sprintf(txtpath, "/home/dimas/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE * list;
    list = fopen(txtpath, "w");

    Player kiper[1];
    Player bek_players[9];
    Player gelandang_players[9];
    Player penyerang_players[9];

    int kiper_count = 0, bek_count = 0, gelandang_count = 0, penyerang_count = 0;

    list_players("Kiper", kiper, & kiper_count);
    list_players("Bek", bek_players, & bek_count);
    list_players("Gelandang", gelandang_players, & gelandang_count);
    list_players("Penyerang", penyerang_players, & penyerang_count);

    qsort(bek_players, bek_count, sizeof(Player), cmp_func);
    qsort(gelandang_players, gelandang_count, sizeof(Player), cmp_func);
    qsort(penyerang_players, penyerang_count, sizeof(Player), cmp_func);

    if(list != NULL) {
        fprintf(list, "Kiper : %s\n\n", kiper[0].listname);
        fprintf(list, "Bek :\n");
        for (int i = 0; i < bek && i < bek_count; i++) {
            fprintf(list, "%s\n", bek_players[i].listname);
        }
        fprintf(list, "\nGelandang:\n");
        for (int i = 0; i < gelandang && i < gelandang_count; i++) {
            fprintf(list, "%s\n", gelandang_players[i].listname);
        }
        fprintf(list, "\nPenyerang:\n");
        for (int i = 0; i < penyerang && i < penyerang_count; i++) {
            fprintf(list, "%s\n", penyerang_players[i].listname);
        }
        fclose(list);
    }
}

void categorize(const char * search_pattern,
    const char * destination) {
    if (fork() == 0) {
        chdir("./players");
        execl("/usr/bin/find", "find", ".", "-name", search_pattern, "-exec", "mv", "{}", destination, ";", NULL);
    }
    while ((wait(&status3)) > 0);
}

int main(){
    pid_t char_ch;
    char_ch = fork();
    if(char_ch < 0){
        exit(EXIT_FAILURE);
    }else if(!char_ch){
        execl("/usr/bin/wget", "wget", "-O", "./players.zip", "https://drive.google.com/u/0/uc?export=download&confirm=0ZCx&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "-qq", NULL);
    }else{
        wait(0);
        char_ch = fork();
        if(char_ch < 0){
            exit(EXIT_FAILURE);
        }else if(!char_ch){
            char *argv[] = {"unzip", "/home/dimas/SISOP/praktikum2/soal3/players.zip", "-d", "/home/dimas/SISOP/praktikum2/soal3", NULL};
            execv("/usr/bin/unzip", argv);
        }else{
            wait(0);
            char_ch = fork();
            if(char_ch < 0){
                exit(EXIT_FAILURE);
            }else if(!char_ch){
                execl("/bin/rm", "rm", "/home/dimas/SISOP/praktikum2/soal3/players.zip", NULL);
            }else{
                wait(0);
                char_ch = fork();
                if(char_ch < 0){
                    exit(EXIT_FAILURE);
                }else if(!char_ch){
                    DIR *dp;
                    struct dirent *ep;
                    dp = opendir("./players");

                    if(dp){
                        while ((ep = readdir (dp)) != NULL){
                            if (strcmp(ep->d_name, ".") == 0 || strcmp(ep->d_name, "..") == 0) continue;
                            char namafile[350];
                            sprintf(namafile, "/home/dimas/SISOP/praktikum2/soal3/players/%s", ep->d_name);
                            if(strstr(ep->d_name, "ManUtd") == NULL){
                                if(!fork()){
                                    execl("/bin/rm", "rm", namafile, NULL);
                                }
                            }
                            while ((wait(&status)) > 0);
                        }
                        closedir(dp);
                        while ((wait(&status2)) > 0);
                    }
                }else{
                    wait(0);
                    char_ch = fork();
                    if(char_ch < 0){
                        exit(EXIT_FAILURE);
                    }else if(!char_ch){
                        execl("/bin/mkdir", "mkdir", "-p", "./players/Kiper", "./players/Bek", "./players/Gelandang", "./players/Penyerang", NULL);
                    }else{
                        wait(0);
                    	char_ch = fork();
                    	if(char_ch < 0){
                            exit(EXIT_FAILURE);
                    	}else if(!char_ch){
                    	    categorize("*_Bek_*", "./Bek");
                            categorize("*_Gelandang_*", "./Gelandang");
                            categorize("*_Kiper_*", "./Kiper");
                            categorize("*_Penyerang_*", "./Penyerang");
                    	}else{
                    	    wait(0);
                            int bek, gelandang, penyerang;
                            printf("Enter the number of Bek, Gelandang, Penyerang: ");
                            scanf("%d %d %d", &bek, &gelandang, &penyerang);
                            buatTim(bek, gelandang, penyerang);
                    	}
                    }
                }
            }
        }
    }
}
